package com.example.jeezoo.fight.exposition.request;

import javax.validation.constraints.NotNull;

public final class FightByIdRequest {
    @NotNull
    public Long id;
}
