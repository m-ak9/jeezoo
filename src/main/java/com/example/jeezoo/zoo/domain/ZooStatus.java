package com.example.jeezoo.zoo.domain;

import com.example.jeezoo.kernel.annotations.ValueObject;

@ValueObject
public enum ZooStatus {
    Open,Close
}
