package com.example.jeezoo.animal.infrastructure.primary.request;

public class UpdateAnimalRequest {
    public String    name;
    public String    type;
    public String    status;
    public Long      spaceId;
}
