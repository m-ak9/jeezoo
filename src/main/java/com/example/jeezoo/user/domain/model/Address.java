package com.example.jeezoo.user.domain.model;

public interface Address {

  String city();

  String street();

  String country();

  String zipCode();

  String toString();
}
